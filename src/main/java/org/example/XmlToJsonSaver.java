package org.example;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import mincomsvyaz.esia.reg_service.find_account._1_4.ESIAFindAccountRequestType;
import mincomsvyaz.esia.reg_service.find_account._1_4.ESIAFindAccountResponseType;

import javax.xml.bind.JAXBException;
import javax.xml.bind.ValidationException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class XmlToJsonSaver {
    public void save(String xmlFilePath, String resultPathFile) throws IOException, JAXBException {
        String xmlString = Files.readString(Paths.get(xmlFilePath), StandardCharsets.UTF_8);

        ESIAFindAccountRequestType request = new Converter().unmarshall(xmlString, ESIAFindAccountRequestType.class);
        ESIAFindAccountResponseType response = new Converter().unmarshall(xmlString, ESIAFindAccountResponseType.class);

        if (request.getFirstName() != null) {
            ObjectMapper jsonMapper = new ObjectMapper();
            jsonMapper.setPropertyNamingStrategy(PropertyNamingStrategies.UPPER_CAMEL_CASE);

            ObjectWriter writer = jsonMapper.writer(new DefaultPrettyPrinter());
            writer.writeValue(new File(resultPathFile + "request.json"), request);
        } else if (response.getAccount().size() != 0 || !response.getErrorStatusInfo().isEmpty()) {
            ObjectMapper jsonMapper = new ObjectMapper();
            jsonMapper.setPropertyNamingStrategy(PropertyNamingStrategies.UPPER_CAMEL_CASE);

            ObjectWriter writer = jsonMapper.writer(new DefaultPrettyPrinter());
            writer.writeValue(new File(resultPathFile + "response.json"), response);
        } else {
            throw new ValidationException("Xml not valid by ESIAFindAccountResponse or ESIAFindAccountRequest");
        }
    }
}
