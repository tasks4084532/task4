package org.example;

import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;

@Slf4j
public class Converter {
    public <T> T unmarshall(String xmlString, Class<T> classType) throws JAXBException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(classType);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            return unmarshaller.unmarshal(new StreamSource(new StringReader(xmlString)), classType).getValue();
        } catch (UnmarshalException ex) {
            log.error(ex.getMessage());
            throw new IllegalArgumentException("Could not unmarshall xml string");
        }
    }
}