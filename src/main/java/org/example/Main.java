package org.example;

import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.JAXBException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Slf4j
public class Main {

    public static void main(final String[] arguments) throws IOException, JAXBException {
        Properties inputData = new Properties();
        String resultPathFile;
        String xmlFilePath;
        try (FileInputStream inputFile = new FileInputStream("./src/main/resources/application.properties")) {
            inputData.load(inputFile);
            resultPathFile = inputData.get("filePath").toString();
            xmlFilePath = inputData.get("xmlFilePath").toString();
        }
        new XmlToJsonSaver().save(xmlFilePath, resultPathFile);
    }
}