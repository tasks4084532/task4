import org.example.XmlToJsonSaver;

import javax.xml.bind.JAXBException;
import javax.xml.bind.ValidationException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class TestConvert {
    @org.junit.Test
    public void Test() throws IOException, JAXBException {
        try {
            new XmlToJsonSaver().save("C:/Users/akast/OneDrive/Desktop/Java/task4/data/pos_response.xml", "C:/Users/akast/OneDrive/Desktop/Java/task4/");
            fail();
        } catch (ValidationException ignored) {
        }

        File fileJsonPosResponse = new File("C:/Users/akast/OneDrive/Desktop/Java/task4/pos_response.json");
        assertFalse(fileJsonPosResponse.exists());

        new XmlToJsonSaver().save("C:/Users/akast/OneDrive/Desktop/Java/task4/data/request.xml", "C:/Users/akast/OneDrive/Desktop/Java/task4/");
        File fileJsonRequest = new File("C:/Users/akast/OneDrive/Desktop/Java/task4/request.json");
        assertTrue(fileJsonRequest.exists() && !fileJsonRequest.isDirectory());

        new XmlToJsonSaver().save("C:/Users/akast/OneDrive/Desktop/Java/task4/data/response.xml", "C:/Users/akast/OneDrive/Desktop/Java/task4/");
        File fileJsonResponse = new File("C:/Users/akast/OneDrive/Desktop/Java/task4/response.json");
        assertTrue(fileJsonResponse.exists() && !fileJsonResponse.isDirectory());
    }
}
