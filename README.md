Написать программу по конвертации данных из xml формата в json формат (нейминг любой из прошлых задач).
Данные на вход поступают из файла в формате xml, путь которого передается в аргументе.
Каталог для выходного файла также поступает в аргументе. Данные, которые поступают на вход, должны соответствовать приложенным XSD.
В случае несоответствия - исключение, программа прерывается.
Написать юнит тест на разработанный функционал.

Задача проверяет навыки:
- Обработка исключений
- Работа с ФС из java
- Понимание формата json и умение работать с ним с помощью библиотеки Jackson
- Понимание форматов xml и xsd, умение работать с ними, умение получить pojo (plain old java object)
- Написание юнит тестов